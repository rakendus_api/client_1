function getCompanies() {
  fetch('http://localhost:8080/companies')
    /*.then(
      function(response) {
        return response.json();
      }
    )*/
    .then(response => response.json()) // funktsioon, mis võtab parameetrina vastu teise funktsiooni, et seda vastavalt vajadusele käivitada.
    //.then (prepareDisplayCompanies);
    .then(companies => displayCompanies(companies));
}

function deleteCompany(id) {
  if (confirm('Oled kindel?')) {
    const deleteUrl = "http://localhost:8080/company/" + id;
    fetch(deleteUrl, {
        method: 'DELETE'
      })
      .then(response => getCompanies());
  }
}

function addOrEditCompany() {
  if (validateCompany()) {
    const id = document.getElementById('companyID').value;
    if (id > 0) {
      editCompany();

    } else {
      addCompany();
    }
  }

}

function validateCompany() {
  const name = document.getElementById('companyName').value;
  const logo = document.getElementById('companyLogo').value;

  if (name == null || name.length < 1) {
    displayCompanyValidationError("Ettevõte nimi puudulik!");
    return false;
  }
  if (logo == null || logo.length < 1) {
    displayCompanyValidationError("Logo puudulik!");
    return false;
  }
  hideCompanyValidationError();
  return true;
}

function addCompany() {
  const name = document.getElementById('companyName').value;
  const logo = document.getElementById('companyLogo').value;

  const addUrl = 'http://localhost:8080/company';
  fetch(addUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "name": name,
        "logo": logo
      })
    })
    .then(response => {
      getCompanies();
      closeCompanyModal();
    });
}

function editCompany() {
  const id = document.getElementById('companyID').value;
  const name = document.getElementById('companyName').value;
  const logo = document.getElementById('companyLogo').value;

  const editUrl = 'http://localhost:8080/company';
  fetch(editUrl, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "id": id,
        "name": name,
        "logo": logo
      })
    })
    .then(response => {
      getCompanies();
      closeCompanyModal();
    });
}



function displayCompanies(companies) {
  let companyList = document.getElementById('companyList');
  companyList.innerHTML = "";

  for (let i = 0; i < companies.length; i++) {
    let companyRow = "";
    companyRow = "<tr>";
    companyRow = companyRow + "<td>" + companies[i].id + "</td>";
    companyRow = companyRow + "<td>" + companies[i].name + "</td>";
    companyRow = companyRow + "<td>" + '<img height="80" src="' + companies[i].logo + '" />' + "</td>";
    companyRow = companyRow + '<td>';
    companyRow = companyRow + '<button type="button" class="btn btn-danger" onClick="deleteCompany(' + companies[i].id + ')">Kustuta<Danger</button><div>';
    companyRow = companyRow + '<button type="button" class="btn btn-warning" onClick="openCompanyModal(' + companies[i].id + ')">Muuda<Warning</button>';
    companyRow = companyRow + "</td>";
    companyRow = companyRow + "</tr>";
    companyList.innerHTML += companyRow;
  }
}

function openCompanyModal(id) {

  document.getElementById('companyID').value = null;
  document.getElementById('companyName').value = null;
  document.getElementById('companyLogo').value = null;
  $("#exampleModal").modal("show");

  if (id > 0) {
    const getCompanyUrl = 'http://localhost:8080/company/' + id;
    fetch(getCompanyUrl)
      .then(response => response.json())
      .then(company => {

        document.getElementById('companyID').value = company.id;
        document.getElementById('companyName').value = company.name;
        document.getElementById('companyLogo').value = company.logo;

      });
  }
}

function closeCompanyModal() {
  $("#exampleModal").modal("hide");

}

function displayCompanyValidationError(errorText) {
  let errorDiv = document.getElementById('companyValidationError');
  errorDiv.style.display = 'block';
  errorDiv.innerHTML = errorText;

}

function hideCompanyValidationError(errorText) {
  let errorDiv = document.getElementById('companyValidationError');
  errorDiv.style.display = 'none';
  errorDiv.innerHTML = '';
}